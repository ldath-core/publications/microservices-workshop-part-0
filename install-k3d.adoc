For the local work we will be using k3dfootnote:[Lightweight wrapper to run k3s https://k3d.io/] which just wraps K3s clusters we will be using.

We can install it with a help of brew.

----
brew install k3d
----

Commands connected to the K3s clusters management can be found just by using `--help` by example: `k3d cluster --help` will show available commends for the `cluster` command.

[source,text]
----
Manage cluster(s)

Usage:
  k3d cluster [flags]
  k3d cluster [command]

Available Commands:
  create      Create a new cluster
  delete      Delete cluster(s).
  edit        [EXPERIMENTAL] Edit cluster(s).
  list        List cluster(s)
  start       Start existing k3d cluster(s)
  stop        Stop existing k3d cluster(s)

Flags:
  -h, --help   help for cluster

Global Flags:
      --timestamps   Enable Log timestamps
      --trace        Enable super verbose output (trace logging)
      --verbose      Enable verbose output (debug logging)

Use "k3d cluster [command] --help" for more information about a command.
----

You will be most probably using often `start` and `stop` commands to be sure your machine resources are in the optimum use and `create` when creating new clusters.
